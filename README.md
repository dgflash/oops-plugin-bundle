#### AB包资源路径管理插件
自动生成AB包内资源路径，提供代码智能提示

assets/bundle文件夹下，文件夹或者文件命名前缀带$符号，会跳过路径写入

assets\script\game\common\bundle\BundleConfig.ts 路径写入配置文件

assets\script\game\common\bundle\BundleManager.ts 资源加载调用文件

assets\demo\bundle 提供演示demo
温馨提示：插件安装好后，按F8可快速更新配置文件

#### 兼容版本
- Cocos Creator 3.x